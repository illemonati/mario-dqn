import torch
import torchvision

from PIL import Image
import numpy as np
from collections import deque

import gym
from gym.wrappers import FrameStack
from nes_py.wrappers import JoypadSpace
import gym_super_mario_bros


from gray_scale import GrayScaleObservation
from resize import ResizeObservation
from skip_frame import SkipFrame
from agent import Agent

from pathlib import Path
from logger import MetricLogger

import datetime


env = gym_super_mario_bros.make('SuperMarioBros-1-1-v0')
env = JoypadSpace(env, [["right"], ["right", "A"]])

env.reset()
next_state, reward, done, info = env.step(action=0)
print(next_state.shape, reward, done, info)


env = SkipFrame(env, skip=2**2)
env = GrayScaleObservation(env)
env = ResizeObservation(env, (84, 84))
env = FrameStack(env, num_stack=4)


use_cuda = torch.cuda.is_available()
print(f"Using CUDA: {use_cuda}")
print()

save_dir = Path("checkpoints") / \
    datetime.datetime.now().strftime("%Y-%m-%dT%H-%M-%S")
save_dir.mkdir(parents=True)

mario = Agent((4, 84, 84), env.action_space.n, save_dir=save_dir)
mario.load('checkpoints/2021-03-21T23-08-47/mario_8200000.chkpt')
print(mario.epsilon)

logger = MetricLogger(save_dir)

episodes = int(4e4)
for e in range(episodes):

    state = env.reset()

    # Play the game!
    while True:

        # Run agent on the state
        action = mario.act(state)

        if e % 10 == 0:
            env.render()

        # Agent performs action
        next_state, reward, done, info = env.step(action)

        # Remember
        mario.add_to_memory((state, next_state, action, reward, done))

        # Learn
        q, loss = mario.learn()

        # Logging
        logger.log_step(reward, loss, q)

        # Update state
        state = next_state

        # Check if end of game
        if done or info["flag_get"]:
            break

    logger.log_episode()

    if e % 10 == 0:
        logger.record(episode=e, epsilon=mario.epsilon,
                      step=mario.current_step)
