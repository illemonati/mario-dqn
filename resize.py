import gym
from gym.spaces import Box
from torchvision import transforms as T
import numpy as np


class ResizeObservation(gym.ObservationWrapper):
    def __init__(self, env, shape):
        super().__init__(env)
        self.shape = tuple(shape)

        observation_shape = self.shape + self.observation_space.shape[:-1]
        self.observation_space = Box(
            low=0, high=255, shape=observation_shape, dtype=np.uint8)

    def observation(self, observation):
        transforms = T.Compose(
            [
                T.Resize(self.shape),
                T.Normalize(0, 255)
            ]
        )
        observation = transforms(observation).squeeze(0)
        return observation
