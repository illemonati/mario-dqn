from torch import nn


class DQN(nn.Module):
    def __init__(self, input_shape, output_shape):
        super().__init__()
        color, height, width = input_shape
        self.online = self.create_model(color, output_shape)
        self.target = self.create_model(color, output_shape)

        for param in self.target.parameters():
            param.requires_grad = False

    @staticmethod
    def create_model(in_channels, output_shape):
        return nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=32,
                      kernel_size=8, stride=4),
            nn.ReLU(),
            nn.Conv2d(in_channels=32, out_channels=64,
                      kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(in_channels=64, out_channels=64,
                      kernel_size=3, stride=1),
            nn.ReLU(),
            nn.Flatten(),
            nn.Linear(3136, 512),
            nn.ReLU(),
            nn.Linear(512, output_shape),
        )

    def forward(self, input, model):
        if model == 'online':
            return self.online(input)
        elif model == 'target':
            return self.target(input)
