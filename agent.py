import torch
import numpy as np
from collections import deque
import random
from dqn import DQN


class Agent:
    def __init__(self, state_shape, action_amount, save_dir):
        self.state_shape = state_shape
        self.action_amount = action_amount
        self.save_dir = save_dir
        self.use_cuda = torch.cuda.is_available()

        self.net = DQN(self.state_shape, self.action_amount)
        if self.use_cuda:
            self.net = self.net.to(device='cuda')

        self.epsilon = 1
        self.epsilon_decay = 0.99999975
        self.epsilon_min = 0.1
        self.current_step = 0

        self.save_interval = 5e4

        self.memory = deque(maxlen=int(2e4))
        self.batch_size = 32

        self.gamma = 0.9

        self.optimizer = torch.optim.Adam(self.net.parameters(), lr=0.00025)
        self.loss_fn = torch.nn.SmoothL1Loss()

        self.burnin = 1e4
        self.learn_interval = 3
        self.sync_interval = 1e4

    def act(self, state):
        action = -1
        state = state.__array__()
        if np.random.rand() < self.epsilon:
            action = np.random.randint(self.action_amount)
        else:
            state = torch.tensor(state)
            if self.use_cuda:
                state = state.cuda()

            state = torch.unsqueeze(state, 0)
            action_prediction = self.net(state, model='online')
            action = torch.argmax(action_prediction, axis=1).item()

        self.epsilon = max(self.epsilon_min, self.epsilon * self.epsilon_decay)
        self.current_step += 1
        return action

    def add_to_memory(self, experience):
        state, next_state, action, reward, done = experience
        state, next_state = (x.__array__() for x in (state, next_state))
        action, reward, done = ([x] for x in (action, reward, done))
        experience = (state, next_state, action, reward, done)
        experience = tuple(torch.tensor(x) for x in experience)
        # if self.use_cuda:
        #     experience = tuple(x.cuda() for x in experience)

        self.memory.append(experience)

    def sample_memory(self):
        batch = random.sample(self.memory, self.batch_size)
        experiences = (
            torch.stack(x) for x in zip(*batch))

        experiences = (x.cuda() for x in experiences)

        state, next_state, action, reward, done = experiences
        return state, next_state, action.squeeze(), reward.squeeze(), done.squeeze()

    def td_estimate(self, state, action):
        Q = self.net(state, model='online')[
            np.arange(0, self.batch_size),
            action
        ]
        return Q

    @torch.no_grad()
    def td_target(self, reward, next_state, done):
        next_state_Q = self.net(next_state, model='online')
        best_action = torch.argmax(next_state_Q, axis=1)
        next_Q = self.net(next_state, model='target')[
            np.arange(0, self.batch_size),
            best_action
        ]
        return (reward + (1-done.float()) * self.gamma * next_Q).float()

    def update_Q_online(self, td_estimate, td_target):
        loss = self.loss_fn(td_estimate, td_target)
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()
        return loss.item()

    def sync_Q_target(self):
        self.net.target.load_state_dict(self.net.online.state_dict())

    def save(self):
        torch.save(
            {
                'model': self.net.state_dict(),
                'epsilon': self.epsilon
            },
            f'{self.save_dir}/mario_{self.current_step}.chkpt'
        )
        print(f'saved at step {self.current_step}')

    def load(self, path):
        save_data = torch.load(path)
        # print(save_data)
        self.net.load_state_dict(save_data['model'])
        self.epsilon = save_data['epsilon']

    def learn(self):
        if self.current_step % self.sync_interval == 0:
            self.sync_Q_target()

        if self.current_step % self.save_interval == 0:
            self.save()

        if self.current_step < self.burnin:
            return None, None

        if self.current_step % self.learn_interval != 0:
            return None, None

        state, next_state, action, reward, done = self.sample_memory()
        td_estimate = self.td_estimate(state, action)
        td_target = self.td_target(reward, next_state, done)

        loss = self.update_Q_online(td_estimate, td_target)

        return (td_estimate.mean().item(), loss)
