import gym


class SkipFrame(gym.Wrapper):
    def __init__(self, env, skip):
        super().__init__(env)
        self.skip = skip

    def step(self, action):
        total_reward = 0
        done = False
        info = None
        next_state = None
        for i in range(self.skip):
            next_state, reward, done, info = self.env.step(action)
            total_reward += reward
            if done:
                break
        return next_state, total_reward, done, info
