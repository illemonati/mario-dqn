import gym
from gym.spaces import Box
import numpy as np
import torch
from torchvision import transforms as T


class GrayScaleObservation(gym.ObservationWrapper):
    def __init__(self, env):
        super().__init__(env)
        no_color_observation_shape = self.observation_space.shape[:-1]
        self.observation_space = Box(
            low=0, high=255, shape=no_color_observation_shape, dtype='uint8')

    def permute_observation(self, observation):
        # height, width, color -> color, height, width
        observation = np.transpose(observation, (2, 0, 1))
        observation = torch.tensor(observation.copy(), dtype=torch.float32)
        return observation

    def observation(self, observation):
        observation = self.permute_observation(observation)
        observation = T.Grayscale()(observation)
        return observation
